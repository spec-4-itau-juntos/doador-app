package br.com.itau.doador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoadorAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoadorAppApplication.class, args);
    }

}
