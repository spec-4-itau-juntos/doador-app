package br.com.itau.doador.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
public class DoadorController {

    @GetMapping("/get")
    public ResponseEntity get() {
        return ResponseEntity.ok(new Object());
    }

    @PostMapping("/post")
    public ResponseEntity post(@RequestBody Object object) {
        return ResponseEntity.created(URI.create("")).body(object);
    }

    @PatchMapping("/{id}")
    public ResponseEntity patch(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(new Object());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        return ResponseEntity.ok().body(new Object());
    }
}
